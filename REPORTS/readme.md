colour images were removed from the phylogeny to avoid copyright infringement.
I could contact copyright holders to get permission to use the photos


"

Embedded image credits are as follows: Trichophorum cespitosum (Linnaeus) Schur by Mihai Costea, copyright Mihai Costea, http://phytoimages.siu.edu/imgs/Cusman1/r/Cyperaceae_Trichophorum_alpinum_92024.html; Carex macrocephala Willdenow ex Sprengel taken by Daniel Mosquin, distributed under CC BY-NC-SA 4.0, https://botanyphoto.botanicalgarden.ubc.ca/2013/07/carex-macrocephala/; Carex capitata Linnaeus by Biopix: JC Schou; Carex grayi J. Carey by"

See http://phytoimages.siu.edu/index.html

References that were not embedded in the text using Zotero were placed in the file after the fact

Figures need to be reformatted

The tree figure was resized in Preview
https://support.apple.com/en-ca/guide/preview/prvw2015/mac#:~:text=Reduce%20an%20image's%20file%20size,is%20shown%20at%20the%20bottom.