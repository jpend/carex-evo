Summary/meaning of results (paragraph 1):
-the high diversity of North American sedges is related to their ability to explore ecological and morphological space
-the diversity of sedges and the importance of understanding how ecological diversity and lineage diversity are interconnected.

1. Assemblages of Carex diversity in North America north of Mexico
-assemblages as groups of species that may have migrated to NAm or speciated in situ
-implications and limitations of continental scale
-comparison with Spalink 2018 NAm paper and his global paper, spatial structure of Carex diversity
-Mexico dynamics as separate entity

2. Correlation between rates of trait evolution and species diversity in Carex
-perigynium hypothesis within context of our work, no correlation found in vegetative morphology and strongest correlation with reproductive morphology
-chromosome hypothesis within context of our work
-Habitat heterogeneity/ecological diversity within context of our work
-Limtations of interpretation
-Discus future direction that can extend the contribution, leveraging a global phylogeny that can address the limitations of this work

3. Rates of trait evolution as drivers of diversification in Carex
-ecological speciation may be a major player in Carex
-discuss future directions

4. Rates of trait evolution implication in diversification across the tree of life
-paragraph from thesis
-how our work contributes to this body of work
-clever way of perceiving the contribution within this body of work

5. Future research priorities
How the paper moves the field forward (final paragraph):
-by providing these new opportunities (discuss)
-mechanistic investigations re: the relationship we've shown btw trait evolution & species richness
-relationship between chromosome dynamics and rates of trait evo

——
Oct. 2020

