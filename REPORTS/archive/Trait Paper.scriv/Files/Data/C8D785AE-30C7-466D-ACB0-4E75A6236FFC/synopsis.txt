Field gap: (paragraph 1)
-Asymmetrical patterns of diversity across tree of life. 
-Historical relevance of static traits and diversification, for example key innovations
-Dynamic traits and correlations with rates of diversification, incipient understanding of complexity of diversification processes
-Researchers continue to test for correlation between dynamic traits and diversification, but results across taxonomy and geography have been inconsistent. How does trait evolution affect diversity? = field gap

Subfield gap: (paragraph 2)
-Carex is an example of a radiation with a reverse latitudinal gradient
-Describe the genus and its relevance, habitat heterogeneity, etc.
-Currently accepted hypotheses include perigynium as important, cooling climate, holocentric chromosomes, etc.
-Why are there so many species in NAm sedges? = Subfield gap

Yet narrower paper gap: (paragraph 3)
-A non-exclusive yet critical component to its success may lie in its dynamism in niche. 
-Are rates of niche exploration related to lineage-level diversity at continental scale? = Gap we will fill

Paper summary: (paragraph 4)
Our approach -
Our results -
The clades that have navigated more of morpho- and ecospace exhibit higher diversity. 
The close correlation demonstrates that filling ecological and morphological space is part of the process of lineage diversification in sedges.
