library(tidyverse)
setwd("../Documents/School/MASTERS PUBLICATION REWRITES/TRAIT PAPER/carex-msc/DATA/molecular/sequence-submission")
olddata_long <- read_csv("genbank_file.csv")
olddata_long_id <- rep(1:1298, each=19)
olddata_long <- cbind(olddata_long, olddata_long_id)
data_wide <- olddata_long %>% pivot_wider(names_from = data_type, values_from = data)
data_wide <- data_wide %>% select(c(LOCUS,DEFINITION,ACCESSION,VERSION,SOURCE,ORGANISM,COMMENT,FEATURES)) # only keep certain columns
data_wide <- data_wide %>% unnest
write_csv(data_wide, "genbank_file_wide.csv")
